#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <string.h>
#define MAXD 1001

using namespace std;

class RemovingDigits {
public:
  string maxNumber(string, string);
};

int a[MAXD], p[MAXD];
int s[10];
int v[MAXD];
int n, m;

int cmp(const int & i, const int & j)
{
    if (a[i] > a[j]) return 1;
    if (a[i] < a[j]) return 0;
    if (i < j) return 1;
    return 0;
}

int check(int pos, int last)
{
    int ss[10];
    for (int i = 1; i <= 9; i++) ss[i] = s[i];
    for (int i = last + 1; i < pos; i++) if (v[i] == 0) {
        ss[a[i]]--;
        if (ss[a[i]] < 0) return 0;
    }

    for (int i = pos + 1; i <= n; i++) ss[a[i]]--;
    for (int i = 1; i <= 9; i++) if (ss[i] > 0) return 0;
    return 1;
}

string RemovingDigits::maxNumber(string number, string digits) {

  string ans;

  memset(s, 0, sizeof(s));
  for (int i = 0; i < digits.length(); i++) s[digits[i] - '0']++;


  n = number.size(), m = digits.length();
  for (int i = 0; i < number.size(); i++) a[i + 1] = number[i] - '0';

  for (int i = 1; i <= n; i++) p[i] = i;
  sort(p + 1, p + n + 1, cmp);

  int pos = 0;
  memset(v, 0, sizeof(v));
  for (int i = 1; i <= n - m; i++) {
      int j;
        for (j = 1; j <= n; j++)
          if ((p[j] > pos) && (check(p[j], pos))) break;


        for (int i = pos + 1; i < p[j]; i++) if (v[i] == 0) {
            v[i] = 1;
            s[a[i]]--;
        }

        pos = p[j];
        ans = ans + (char)(a[pos] + '0');
    }
    return ans;
}

int main() {
    string number, digits;
	cin>>number>>digits;
	RemovingDigits r;
	cout<<r.maxNumber(number,digits)<<'\n';
	return 0;
}

