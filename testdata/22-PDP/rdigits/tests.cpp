#include<cstdio>
#include<cstdlib>
#include<ctime>

int s[10];

int main() {
	srand(time(0));

	int i,N,M,t,x;
	for(t=0;t<=10;t++) {
		N = t*50;
		if(N==0) N=15;
		M = rand()%(N-6) + 3;
		
		char name[50];
		sprintf(name,"rdigits.in%d",t+1);
		freopen(name,"w",stdout);
		for(i=0;i<10;i++) s[i] = 0;
		for(i=1;i<N;i++) {
			x = rand()%9 + 1;
			s[x]++;
			printf("%d",x);
		}
		printf("\n");
		i = 0;
		do {
			x = rand()%9+1;
			if(s[x] == 0) continue;
			s[x]--;
			printf("%d",x);
			i++;
		} while(i<M);
		printf("\n");
	}
	return 0;
}

