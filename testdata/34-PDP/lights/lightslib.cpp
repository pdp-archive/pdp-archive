#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <vector>

#include "lightslib.h"

using namespace std;

#undef DEBUG

#ifdef DEBUG
#define debug(...) fprintf(stderr, __VA_ARGS__)
#else
#define debug(...) do {} while(false)
#endif

namespace {

class SegmentTree {
  // Adapted from: https://cp-algorithms.com/data_structures/segment_tree.html
public:
  SegmentTree() : n(0) {}
  explicit SegmentTree(const std::vector<int> &a)
      : n(a.size()), t(4 * a.size()) {
    build(a, 1, 0, n - 1);
  }

  void build(const std::vector<int> &a) {
    n = a.size();
    t.resize(4 * a.size());
    build(a, 1, 0, n - 1);
  }
  void flip(int l, int r) { flip(1, 0, n - 1, l, r); }
  int count() { return t[1].count; }

private:
  struct info {
    int count;
    bool flipped;
  };

  void build(const std::vector<int> &a, int v, int tl, int tr) {
    assert(1 <= v && v < t.size());
    if (tl == tr) {
      t[v].count = a[tl];
    } else {
      int tm = (tl + tr) / 2;
      build(a, v * 2, tl, tm);
      build(a, v * 2 + 1, tm + 1, tr);
      t[v].count = t[v * 2].count + t[v * 2 + 1].count;
    }
    t[v].flipped = false;
  }

  void lazy(int v, int tl, int tr) {
    if (tl > tr) return;
    assert(1 <= v && v < t.size());
    t[v].flipped = !t[v].flipped;
    t[v].count = tr - tl + 1 - t[v].count;
  }

  void flip(int v, int tl, int tr, int l, int r) {
    if (l > r) return;
    if (l == tl && r == tr) {
      lazy(v, tl, tr);
    } else if (tl <= tr) {
      int tm = (tl + tr) / 2;
      assert(1 <= v && v < t.size());
      if (t[v].flipped) {
        lazy(v * 2, tl, tm);
        lazy(v * 2 + 1, tm + 1, tr);
        t[v].flipped = false;
      }
      flip(v * 2, tl, tm, l, std::min(r, tm));
      flip(v * 2 + 1, tm + 1, tr, std::max(l, tm + 1), r);
      t[v].count = t[v * 2].count + t[v * 2 + 1].count;
    }
  }

  int n;
  std::vector<info> t;
};

#ifdef CONTEST
#define report(...)                                                            \
  do {                                                                         \
    FILE *f = fopen("lights_interact.out", "a");                               \
    fprintf(f, __VA_ARGS__);                                                   \
    fclose(f);                                                                 \
  } while (false)
#else
#define report(...) printf(__VA_ARGS__)
#endif

#define fail(...)                                                              \
  do {                                                                         \
    report(__VA_ARGS__);                                                       \
    exit(0);                                                                   \
  } while (false)

enum state { INIT, START, PLAYING };
state current = INIT;

struct TestInfo { int N, K, Q; };

int T = 0;
vector<TestInfo> test_info;
int test;
SegmentTree st;
int moves;

void expect(state curr, state next) {
  if (current != curr) fail("protocol error\n");
  current = next;
}

void initialize() {
  expect(INIT, START);
#ifdef CONTEST
  FILE *f = fopen("lights_interact.in", "r");
#else
  FILE *f = stdin;
#endif
  unsigned seed;
  fscanf(f, "%d%u", &T, &seed);
  for (int i = 0; i < T; ++i) {
    int N, K, mode;
    fscanf(f, "%d%d%d", &N, &K, &mode);
    int Q = 0;
    switch (mode) {
    case 0: Q = 3 * N; break;
    case 1: Q = N + 1; break;
    }
    assert(Q > 0);
    test_info.push_back({N, K, Q});
  }
#ifdef CONTEST
  fclose(f);
#endif
  test = 0;
  srand(seed); // seed the random number generator
}

void initialize_random(int N, int K) {
  debug("initializing random N=%d, K=%d\n", N, K);
  // Reservoir sampling:
  // https://cs.stackexchange.com/questions/87631/reservoir-sampling-algorithm-probability
  vector<int> r(K);
  // Fill the reservoir array.
  for (int i = 0; i < K; ++i) r[i] = i;
  // Replace elements with gradually decreasing probability.
  for (int i = K; i < N; ++i) {
   int j = rand() % i;
   if (j < K) r[j] = i;
  }
  // Initialize segment tree;
  vector<int> count(N, 0);
  for (int x : r) count[x] = 1;
  st.build(count);
#ifdef DEBUG
  debug("lights:");
  for (int x : r) debug(" %d", x+1);
  debug("\n");
#endif
}

} // namespace

pair<int, int> getNQ() {
  // Initialize once.
  if (T == 0) initialize();
  // Check if there's another test.
  expect(START, PLAYING);
  if (test >= T) fail("no more games, you should have finished\n");
  // Initialize the test case.
  initialize_random(test_info[test].N, test_info[test].K);
  // Return the number of lights and the allowed moves and start playing.
  debug("getNQ() = {%d, %d}\n", test_info[test].N, test_info[test].Q);
  moves = 0;
  return make_pair(test_info[test].N, test_info[test].Q);
}

int move(int l, int r) {
  if (l < 1 || r > test_info[test].N || l > r) fail("invalid parameters\n");
  expect(PLAYING, PLAYING);
  if (++moves > test_info[test].Q) fail("number of moves exceeded\n");
  st.flip(l-1, r-1);
  int answer = st.count();
  debug("move(%d, %d) = %d\n", l, r, answer);
  return answer;
}

bool finish() {
  expect(PLAYING, START);
  if (st.count() > 0) fail("can't finish, there are still lights\n");
  int answer = ++test >= T;
  debug("finish() = %s\n", answer ? "true" : "false");
  debug("%d move(s) required\n", moves);
  report("game %d finished successfully\n", test);
  return answer;
}
