#include <algorithm>
#include <cstdio>
#include <cstring>
#include <map>
#include <set>
#include <tuple>
#include <utility>
#include <vector>

#undef DEBUG
#define TASK "marbles"

using namespace std;

using byte = unsigned char;
using state = vector<byte>;

void terminate(double score) {
  FILE *fans = fopen(TASK ".ans", "wt");
  if (fans == NULL) exit(1);
  fprintf(fans, "%lf\n", score);
  fclose(fans);
  exit(0);
}

void debug(const char *filename, int line, const char *msg) {
#ifdef DEBUG
  printf("%s:%d: %s\n", filename, line, msg);
  printf("Assertion failed, terminating...\n");
#endif
}

#define require(what)                    \
  do {                                   \
    if (!(what)) {                       \
      debug(__FILE__, __LINE__, #what);  \
      terminate(0.0);                    \
    }                                    \
  } while (0)

using byte = unsigned char;
using state = vector<byte>;
const state nil;

// Heuristic: how many numbers are in the wrong place.
byte h(const state &s) {
  byte result = 0;
  for (size_t i = 0; i < s.size(); ++i)
    if (s[i] != i + 1)
      ++result;
  return result;
}

bool sorted(const state &s) {
  for (size_t i = 0; i < s.size(); ++i)
    if (s[i] != i + 1)
      return false;
  return true;
}

vector<pair<int, int>> moves;
set<pair<int, state>> opened;
map<state, tuple<int, int, state, set<pair<int, state>>::iterator>> visited;

state search(const state &start) {
  opened.clear();
  visited.clear();
  auto k = opened.insert(make_pair(h(start), start)).first;
  visited[start] = make_tuple(0, 0, nil, k);
  while (!opened.empty()) {
    state x = opened.begin()->second;
    opened.erase(opened.begin());
    get<3>(visited[x]) = opened.end();

    if (sorted(x)) return x;

    int dist = get<0>(visited[x]) + 1;
    state t = x;
    for (size_t i = 0; i < moves.size(); ++i) {
      swap(x[moves[i].first], x[moves[i].second]);
      auto j = visited.find(x);
      if (j == visited.end() || dist < get<0>(j->second)) {
        if (j != visited.end()) {
          auto old_k = get<3>(j->second);
          if (old_k != opened.end()) opened.erase(old_k);
        }
        auto k = opened.insert(make_pair(dist + h(x), x)).first;
        visited[x] = make_tuple(dist, i, t, k);
      }
      swap(x[moves[i].first], x[moves[i].second]);
    }
  }
  return nil;
}

int main(int argc, char *argv[]) {
  const char *file_in = argc > 1 ? argv[1] : TASK ".in";
  const char *file_out = argc > 2 ? argv[2] : TASK ".out";
  int scn;
  FILE *fin = fopen(file_in, "rt");
  require(fin != NULL);
  FILE *fout = fopen(file_out, "rt");
  require(fout != NULL);
  int T, N;
  scn = fscanf(fin, "%d%d", &T, &N);
  require(scn == 2);
  for (int t = 0; t < T; ++t) {
    int M;
    scn = fscanf(fin, "%d", &M);
    require(scn == 1);
    state start;
    for (int i = 0; i < N; ++i) {
      int a;
      scn = fscanf(fin, "%d", &a);
      require(scn == 1);
      start.push_back(a);
    }
    moves.clear();
    for (int i = 0; i < M; ++i) {
      int a, b;
      scn = fscanf(fin, "%d%d", &a, &b);
      require(scn == 2);
      moves.push_back(make_pair(a - 1, b - 1));
    }

    state s = search(start);

    char answer[20];
    scn = fscanf(fout, "%20s", answer);
    require(scn == 1);
    
    if (s != nil) {
      int correct_result = get<0>(visited[s]);
      require(strcmp(answer, "MOVES") == 0);
      int answer_moves;
      scn = fscanf(fout, "%d\n", &answer_moves);
      require(scn == 1);
      require(answer_moves == correct_result);
      state s = start;
      for (int i = 0; i < answer_moves; ++i) {
        int move;
        scn = fscanf(fout, "%d\n", &move);
        require(scn == 1);
        require(1 <= move && move <= M);
        --move;
        swap(s[moves[move].first], s[moves[move].second]);
      }
      require(sorted(s));
    } else {
      require(strcmp(answer, "IMPOSSIBLE") == 0);
      scn = fscanf(fout, "\n");
      require(scn == 0);
    }
  }
  terminate(1.0);
}
