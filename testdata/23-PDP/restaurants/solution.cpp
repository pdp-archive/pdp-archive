#include<stdio.h>
#include<stdlib.h>

int A[1000000];
int B[1000000];
int C[1000000];
int N;

int max(int a,int b){return a>b?a:b;}

int main() {
	int i,b[3],nb[3];
	scanf("%d",&N);
	for(i=0;i<N;i++) scanf("%d",&A[i]);
	for(i=0;i<N;i++) scanf("%d",&B[i]);
	for(i=1;i+1<N;i++) scanf("%d",&C[i]);
	b[0] = 0; b[1] = A[0]; b[2] = B[0];
	for(i=1;i<N;i++) {
		nb[0] = max(b[0],b[1]);
		nb[1] = max(b[0]+A[i],b[2]+B[i]);
		nb[2] = max(b[0]+B[i],b[2]+C[i]);
		b[0] = nb[0]; b[1] = nb[1]; b[2] = nb[2];
	}
	printf("%d\n",max(b[0],b[1]));
	return 0;
}
