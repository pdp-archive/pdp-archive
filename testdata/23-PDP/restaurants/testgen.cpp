#include<stdio.h>
#include<stdlib.h>

int A[1000000];
int B[1000000];
int C[1000000];
int N, MAXV=1000;
int T[10] = {2, 5, 10, 15, 20, 100, 200, 2000, 100000, 1000000};
int main() {
	int i,t;
	srand(0);
	for(t=0;t<10;t++) {
		N = T[t];
		for(i=0;i<N;i++) {
			A[i] = rand()%MAXV;
			B[i] = rand()%(A[i]+1);
			C[i] = rand()%(B[i]+1);
		}
		
		char fname[20];
		sprintf(fname,"restaurants.in%d",t+1);
		FILE *fout = fopen(fname,"w");
		
		fprintf(fout,"%d\n",N);
		for(i=0;i<N;i++) fprintf(fout,"%d%c",A[i],(i+1==N)?'\n':' ');
		for(i=0;i<N;i++) fprintf(fout,"%d%c",B[i],(i+1==N)?'\n':' ');
		for(i=1;i+1<N;i++) fprintf(fout,"%d%c",C[i],(i+2==N)?'\n':' ');
	}
	return 0;
}
